# Asistencia Android básico 2019-2

*Del 10 al 14 de Junio de 2019*

## Registrados

| #    | Folio | Nombre                            | Lunes | Martes | Miércoles | Jueves | Viernes |
| ---- | ----- | --------------------------------- | ----- | ------ | --------- | ------ | ------- |
| 1    | 4279  | Yonathan Berith Jaramillo Ramírez |       |        |           |        |         |
| 2    | 4607  | José Carlos Belmont Flores        |       |        |           |        |         |
| 3    | 4293  | David Jonathan Lazaro Perez       |       |        |           |        |         |
| 4    | 4709  | Sergio Daniel Sánchez Torres      |       |        |           |        |         |
| 5    | 4083  | Eduardo Martin Ramirez Rosillo    |       |        |           |        |         |
| 6    | 4401  | Abel Ismal Martinez Velasco       |       |        |           |        |         |
| 7    | 4276  | Aurora Reyes Aragón               |       |        |           |        |         |
| 8    | 4467  | Luis Arturo Vélez Quiñonez        |       |        |           |        |         |

## No registrados

| #    | Folio | Nombre | Lunes | Martes | Miércoles | Jueves | Viernes |
| ---- | ----- | ------ | ----- | ------ | --------- | ------ | ------- |
| 1    |       |        |       |        |           |        |         |
| 2    |       |        |       |        |           |        |         |
| 3    |       |        |       |        |           |        |         |
| 4    |       |        |       |        |           |        |         |
| 5    |       |        |       |        |           |        |         |
| 6    |       |        |       |        |           |        |         |
| 7    |       |        |       |        |           |        |         |
| 8    |       |        |       |        |           |        |         |

